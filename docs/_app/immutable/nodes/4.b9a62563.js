import{S as M,i as W,s as B,C as ct,k as p,l as y,m as u,h as d,n as f,b as V,G as h,I as gt,J as ft,K as mt,g as Y,N as pt,Y as ot,d as O,D as U,q as S,a as N,F as H,r as _,c as k,H as et,R as nt,L as yt,e as it,P as vt,Z as at,u as wt,T as Ct,y as q,z as D,A as T,B as z,o as bt}from"../chunks/index.b566ce2b.js";import{a as st,C as St}from"../chunks/ContentRect.c3a0c6ba.js";function _t(l){let t,n,o,e;const i=l[1].default,a=ct(i,l,l[0],null);return{c(){t=p("div"),n=p("h2"),a&&a.c(),this.h()},l(s){t=y(s,"DIV",{class:!0});var r=u(t);n=y(r,"H2",{});var c=u(n);a&&a.l(c),c.forEach(d),r.forEach(d),this.h()},h(){f(t,"class","svelte-9tlwqu")},m(s,r){V(s,t,r),h(t,n),a&&a.m(n,null),e=!0},p(s,[r]){a&&a.p&&(!e||r&1)&&gt(a,i,s,s[0],e?mt(i,s[0],r,null):ft(s[0]),null)},i(s){e||(Y(a,s),pt(()=>{e&&(o||(o=ot(t,st,{},!0)),o.run(1))}),e=!0)},o(s){O(a,s),o||(o=ot(t,st,{},!1)),o.run(0),e=!1},d(s){s&&d(t),a&&a.d(s),s&&o&&o.end()}}}function It(l,t,n){let{$$slots:o={},$$scope:e}=t;return l.$$set=i=>{"$$scope"in i&&n(0,e=i.$$scope)},[e,o]}class Nt extends M{constructor(t){super(),W(this,t,It,_t,B,{})}}function lt(l,t,n){const o=l.slice();return o[4]=t[n],o}function ht(l){let t,n;return{c(){t=p("span"),n=S("Table of Contents"),this.h()},l(o){t=y(o,"SPAN",{class:!0});var e=u(t);n=_(e,"Table of Contents"),e.forEach(d),this.h()},h(){f(t,"class","svelte-14gkq83")},m(o,e){V(o,t,e),h(t,n)},d(o){o&&d(t)}}}function rt(l){let t,n,o=l[0]!=null&&dt(l);return{c(){t=p("div"),n=p("ul"),o&&o.c(),this.h()},l(e){t=y(e,"DIV",{class:!0});var i=u(t);n=y(i,"UL",{class:!0});var a=u(n);o&&o.l(a),a.forEach(d),i.forEach(d),this.h()},h(){f(n,"class","svelte-14gkq83"),f(t,"class","svelte-14gkq83")},m(e,i){V(e,t,i),h(t,n),o&&o.m(n,null)},p(e,i){e[0]!=null?o?o.p(e,i):(o=dt(e),o.c(),o.m(n,null)):o&&(o.d(1),o=null)},d(e){e&&d(t),o&&o.d()}}}function dt(l){let t,n=l[0],o=[];for(let e=0;e<n.length;e+=1)o[e]=ut(lt(l,n,e));return{c(){for(let e=0;e<o.length;e+=1)o[e].c();t=it()},l(e){for(let i=0;i<o.length;i+=1)o[i].l(e);t=it()},m(e,i){for(let a=0;a<o.length;a+=1)o[a]&&o[a].m(e,i);V(e,t,i)},p(e,i){if(i&1){n=e[0];let a;for(a=0;a<n.length;a+=1){const s=lt(e,n,a);o[a]?o[a].p(s,i):(o[a]=ut(s),o[a].c(),o[a].m(t.parentNode,t))}for(;a<o.length;a+=1)o[a].d(1);o.length=n.length}},d(e){vt(o,e),e&&d(t)}}}function ut(l){let t,n,o=l[4].innerHTML+"",e,i,a,s;return{c(){t=p("li"),n=p("a"),e=S(o),a=N(),this.h()},l(r){t=y(r,"LI",{class:!0});var c=u(t);n=y(c,"A",{href:!0,class:!0});var I=u(n);e=_(I,o),I.forEach(d),a=k(c),c.forEach(d),this.h()},h(){f(n,"href",i="#"+l[4].id),f(n,"class","svelte-14gkq83"),f(t,"class",s=at(l[4].tagName)+" svelte-14gkq83")},m(r,c){V(r,t,c),h(t,n),h(n,e),h(t,a)},p(r,c){c&1&&o!==(o=r[4].innerHTML+"")&&wt(e,o),c&1&&i!==(i="#"+r[4].id)&&f(n,"href",i),c&1&&s!==(s=at(r[4].tagName)+" svelte-14gkq83")&&f(t,"class",s)},d(r){r&&d(t)}}}function kt(l){let t,n,o,e,i,a,s,r,c,I,x,m=l[1]&&ht(),g=l[1]&&rt(l);return{c(){t=p("div"),n=p("div"),o=p("div"),e=U("svg"),i=U("style"),a=S(`svg {
						fill: #deddda;
					}
				`),s=U("path"),r=N(),m&&m.c(),c=N(),g&&g.c(),this.h()},l(v){t=y(v,"DIV",{class:!0});var C=u(t);n=y(C,"DIV",{class:!0});var G=u(n);o=y(G,"DIV",{class:!0});var $=u(o);e=H($,"svg",{xmlns:!0,height:!0,viewBox:!0});var P=u(e);i=H(P,"style",{});var j=u(i);a=_(j,`svg {
						fill: #deddda;
					}
				`),j.forEach(d),s=H(P,"path",{d:!0}),u(s).forEach(d),P.forEach(d),$.forEach(d),r=k(G),m&&m.l(G),G.forEach(d),c=k(C),g&&g.l(C),C.forEach(d),this.h()},h(){f(s,"d","M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"),f(e,"xmlns","http://www.w3.org/2000/svg"),f(e,"height","1.25em"),f(e,"viewBox","0 0 448 512"),f(o,"class","btn svelte-14gkq83"),f(n,"class","expand-btn svelte-14gkq83"),f(t,"class","table-of-contents svelte-14gkq83")},m(v,C){V(v,t,C),h(t,n),h(n,o),h(o,e),h(e,i),h(i,a),h(e,s),h(n,r),m&&m.m(n,null),h(t,c),g&&g.m(t,null),I||(x=[et(o,"click",l[3]),et(o,"keydown",l[2])],I=!0)},p(v,[C]){v[1]?m||(m=ht(),m.c(),m.m(n,null)):m&&(m.d(1),m=null),v[1]?g?g.p(v,C):(g=rt(v),g.c(),g.m(t,null)):g&&(g.d(1),g=null)},i:nt,o:nt,d(v){v&&d(t),m&&m.d(),g&&g.d(),I=!1,yt(x)}}}function xt(l,t,n){let{contents:o}=t,e=!1;function i(s){Ct.call(this,l,s)}const a=()=>n(1,e=!e);return l.$$set=s=>{"contents"in s&&n(0,o=s.contents)},[o,e,i,a]}class Vt extends M{constructor(t){super(),W(this,t,xt,kt,B,{contents:0})}}function Gt(l){let t;return{c(){t=S("Setting up NeoVim for Godot")},l(n){t=_(n,"Setting up NeoVim for Godot")},m(n,o){V(n,t,o)},d(n){n&&d(t)}}}function $t(l){let t,n,o,e,i,a,s,r,c,I,x,m,g,v,C,G,$,P,j,A,R,J,E;return n=new Vt({props:{contents:l[0]}}),{c(){t=p("div"),q(n.$$.fragment),o=N(),e=p("div"),i=p("h1"),a=S("How to do it"),s=N(),r=p("h2"),c=S("Why I'm doing it"),I=N(),x=p("h2"),m=S("Why I'm doing it 2"),g=N(),v=p("h2"),C=S("Yeah"),G=N(),$=p("h3"),P=S("Another subsection"),j=N(),A=p("h2"),R=S("Conclusion"),J=S(`
			Recently I’ve been using Godot a bit to mess around with game development and I wanted to have
			autocompletion working with CoC. I looked around and there were no extensions for the Godot LSP.
			So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up If you have CoC working
			correctly already, open NeoVim and run :CocConfig, it’ll open the configuration file for your CoC,
			then just add this to the JSON config: Additional steps You probably want syntax highlighting,
			for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor`),this.h()},l(b){t=y(b,"DIV",{class:!0});var L=u(t);D(n.$$.fragment,L),o=k(L),e=y(L,"DIV",{class:!0});var w=u(e);i=y(w,"H1",{});var F=u(i);a=_(F,"How to do it"),F.forEach(d),s=k(w),r=y(w,"H2",{});var K=u(r);c=_(K,"Why I'm doing it"),K.forEach(d),I=k(w),x=y(w,"H2",{id:!0});var Z=u(x);m=_(Z,"Why I'm doing it 2"),Z.forEach(d),g=k(w),v=y(w,"H2",{});var Q=u(v);C=_(Q,"Yeah"),Q.forEach(d),G=k(w),$=y(w,"H3",{});var X=u($);P=_(X,"Another subsection"),X.forEach(d),j=k(w),A=y(w,"H2",{id:!0});var tt=u(A);R=_(tt,"Conclusion"),tt.forEach(d),J=_(w,`
			Recently I’ve been using Godot a bit to mess around with game development and I wanted to have
			autocompletion working with CoC. I looked around and there were no extensions for the Godot LSP.
			So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up If you have CoC working
			correctly already, open NeoVim and run :CocConfig, it’ll open the configuration file for your CoC,
			then just add this to the JSON config: Additional steps You probably want syntax highlighting,
			for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor Recently I’ve been using Godot a bit to mess around with game development
			and I wanted to have autocompletion working with CoC. I looked around and there were no extensions
			for the Godot LSP. So, to use Godot with NeoVim I just had to connect to their LSP. Setting Up
			If you have CoC working correctly already, open NeoVim and run :CocConfig, it’ll open the configuration
			file for your CoC, then just add this to the JSON config: Additional steps You probably want syntax
			highlighting, for that you have 2 options: you could install the sheerun/vim-polyglot or use habamax/vim-godot
			(which provides syntax highlighting as well as a set of commands allows you to run scenes through
			NeoVim). Setting up the editor`),w.forEach(d),L.forEach(d),this.h()},h(){f(x,"id","something-else"),f(A,"id","conclusion"),f(e,"class","blog-text svelte-1dl10o2"),f(t,"class","content svelte-1dl10o2")},m(b,L){V(b,t,L),T(n,t,null),h(t,o),h(t,e),h(e,i),h(i,a),h(e,s),h(e,r),h(r,c),h(e,I),h(e,x),h(x,m),h(e,g),h(e,v),h(v,C),h(e,G),h(e,$),h($,P),h(e,j),h(e,A),h(A,R),h(e,J),E=!0},p(b,L){const w={};L&1&&(w.contents=b[0]),n.$set(w)},i(b){E||(Y(n.$$.fragment,b),E=!0)},o(b){O(n.$$.fragment,b),E=!1},d(b){b&&d(t),z(n)}}}function Lt(l){let t,n,o,e;return t=new Nt({props:{$$slots:{default:[Gt]},$$scope:{ctx:l}}}),o=new St({props:{$$slots:{default:[$t]},$$scope:{ctx:l}}}),{c(){q(t.$$.fragment),n=N(),q(o.$$.fragment)},l(i){D(t.$$.fragment,i),n=k(i),D(o.$$.fragment,i)},m(i,a){T(t,i,a),V(i,n,a),T(o,i,a),e=!0},p(i,[a]){const s={};a&2&&(s.$$scope={dirty:a,ctx:i}),t.$set(s);const r={};a&3&&(r.$$scope={dirty:a,ctx:i}),o.$set(r)},i(i){e||(Y(t.$$.fragment,i),Y(o.$$.fragment,i),e=!0)},o(i){O(t.$$.fragment,i),O(o.$$.fragment,i),e=!1},d(i){z(t,i),i&&d(n),z(o,i)}}}function Pt(l,t,n){let o=null;return bt(()=>{n(0,o=document.querySelectorAll(".blog-text h1, .blog-text h2, .blog-text h3, .blog-text h4, .blog-text h5, .blog-text h6"))}),[o]}class Et extends M{constructor(t){super(),W(this,t,Pt,Lt,B,{})}}export{Et as component};
